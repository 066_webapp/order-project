import {
  IsInt,
  IsNotEmpty,
  IsPositive,
  Matches,
  MaxLength,
} from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  age: number;

  @Matches(/^[0-9]*$/)
  @MaxLength(10)
  @IsNotEmpty()
  tel: string;

  @MaxLength(1)
  @IsNotEmpty()
  gender: string;
}
