import { Product } from './../products/entities/product.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import { Customer } from 'src/customers/entities/customer.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const customer = await this.customerRepository.findOneBy({
      id: createOrderDto.customerId,
    });
    console.log(customer);
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.orderRepository.save(order);
    // const orderItems: OrderItem[] = await Promise.all(
    //   createOrderDto.orderItems.map(async (od) => {
    //     const orderItem = new OrderItem();
    //     orderItem.amount = od.amount;
    //     orderItem.product = await this.productRepository.findOneBy({
    //       id: od.productId,
    //     });
    //     orderItem.name = orderItem.product.name;
    //     orderItem.price = orderItem.product.price;
    //     orderItem.total = orderItem.price * orderItem.amount;
    //     orderItem.order = order;
    //     return orderItem;
    //   }),
    // );
    // console.log(orderItems);
    // for (const orderItem of orderItems) {
    //   this.orderItemsRepository.save(orderItem);
    //   order.amount += orderItem.amount;
    //   order.total += orderItem.total;
    // }
    // order.orderItems = orderItems;

    // const orderItems: OrderItem[] = [];
    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = od.amount;
      orderItem.product = await this.productRepository.findOneBy({
        id: od.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.total = orderItem.price * orderItem.amount;
      orderItem.order = order;
      await this.orderItemsRepository.save(orderItem);
      order.amount += orderItem.amount;
      order.total += orderItem.total;
    }
    console.log(order);
    await this.orderRepository.save(order);
    return this.orderRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
    // return order;
  }

  findAll() {
    return this.orderRepository.find({ relations: ['customer', 'orderItems'] });
  }

  findOne(id: number) {
    return this.orderRepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.orderRepository.findOneBy({ id: id });
    return this.orderRepository.softRemove(order);
  }
}
