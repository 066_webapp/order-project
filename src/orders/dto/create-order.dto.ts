import { Type } from 'class-transformer';
import { IsPositive, IsNotEmpty, IsInt, ValidateNested } from 'class-validator';

class CreateOrderItemDto {
  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  productId: number;

  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  customerId: number;

  @Type(() => CreateOrderItemDto)
  @ValidateNested({ each: true })
  orderItems: CreateOrderItemDto[];
}
